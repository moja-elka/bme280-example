/*
Connecting the BME280 Sensor:
Sensor              ->  Board
-----------------------------
Vin (Voltage In)    ->  5V
Gnd (Ground)        ->  Gnd
SDA (Serial Data)   ->  A4 on Uno/Pro-Mini, 20 on Mega2560/Due, 2 Leonardo/Pro-Micro
SCK (Serial Clock)  ->  A5 on Uno/Pro-Mini, 21 on Mega2560/Due, 3 Leonardo/Pro-Micro
*/

#include <BME280I2C.h>
#include <EnvironmentCalculations.h>
#include <Wire.h>

#define SERIAL_BAUD 9600

BME280I2C bme;    // Default : forced mode, standby time = 1000 ms
// Oversampling = pressure �1, temperature �1, humidity �1, filter off,

float temp(NAN), hum(NAN), pres(NAN);

// Assumed environmental values:
float referencePressure = 101900;     // Pa local QFF (official meteor-station reading)
float outdoorTemp = 10.0;           // �C  measured local outdoor temp.
float barometerAltitude = 80;      // meters ... map readings + barometer position

BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
BME280::PresUnit presUnit(BME280::PresUnit_Pa);
EnvironmentCalculations::AltitudeUnit envAltUnit  =  EnvironmentCalculations::AltitudeUnit_Meters;
EnvironmentCalculations::TempUnit     envTempUnit =  EnvironmentCalculations::TempUnit_Celsius;

void setup()
{
    Serial.begin(SERIAL_BAUD);

    while (!Serial) {} // Wait

    Wire.begin();

    while (!bme.begin()) {
        Serial.println("Could not find BME280 sensor!");
        delay(1000);
    }

    switch (bme.chipModel()) {
        case BME280::ChipModel_BME280:
            Serial.println("Found BME280 sensor! Success.");
            break;
        case BME280::ChipModel_BMP280:
            Serial.println("Found BMP280 sensor! No Humidity available.");
            break;
        default:
            Serial.println("Found UNKNOWN sensor! Error!");
    }

    Serial.print("Assumed outdoor temperature: ");
    Serial.print(outdoorTemp);
    Serial.write(0xC2);
    Serial.write(0xB0);
    Serial.print("C\nAssumed reduced sea level Pressure: ");
    Serial.print(referencePressure);
    Serial.print("Pa\nAssumed barometer altitude: ");
    Serial.print(barometerAltitude);
    Serial.println("m\n***************************************");

    printHeader(&Serial);
}

void loop()
{
    printBasicData(&Serial);
    printEnvironmentCalculations(&Serial);
    delay(5000);
}

void printHeader(Stream* client)
{
    client->print("Temp: ");
    client->print("\t\tHumidity: ");
    client->print("\tPressure: ");
    client->print("\tAltitude: ");
    client->print("\tDew point: ");
    client->print("\tEquivalent Sea Level Pressure: ");
    client->print("\tHeat Index: ");
    client->println("\tAbsolute Humidity: ");
}

/*
* Read and print basic air parameters
*/
void printBasicData(Stream* client)
{
    bme.read(pres, temp, hum, tempUnit, presUnit);

    //Temperature
    client->print(temp);
    client->write(0xC2);
    client->write(0xB0);
    client->print(String(tempUnit == BME280::TempUnit_Celsius ? 'C' :'F'));
    client->print("\t\t");

    //Humidity
    client->print(hum);
    client->print("% RH");
    client->print("\t");

    //Pressure
    client->print(pres);
    client->print("Pa");
    client->print("\t");
}

/*
* Calculate and print environment data
*/
void printEnvironmentCalculations(Stream* client)
{
    float altitude = EnvironmentCalculations::Altitude(pres, envAltUnit, referencePressure, outdoorTemp, envTempUnit);
    float dewPoint = EnvironmentCalculations::DewPoint(temp, hum, envTempUnit);
    float seaLevel = EnvironmentCalculations::EquivalentSeaLevelPressure(barometerAltitude, temp, pres, envAltUnit, envTempUnit);
    float absHum = EnvironmentCalculations::AbsoluteHumidity(temp, hum, envTempUnit);
    float heatIndex = EnvironmentCalculations::HeatIndex(temp, hum, envTempUnit);

    //Altitude
    client->print(altitude);
    client->print((envAltUnit == EnvironmentCalculations::AltitudeUnit_Meters ? "m" : "ft"));
    client->print("\t\t");

    //Dew point
    client->print(dewPoint);
    client->write(0xC2);
    client->write(0xB0);
    client->print(String(envTempUnit == EnvironmentCalculations::TempUnit_Celsius ? "C" :"F"));
    client->print("\t\t");

    //Equivalent Sea Level Pressure
    client->print(seaLevel);
    client->print(String( presUnit == BME280::PresUnit_hPa ? "hPa" :"Pa")); // expected hPa and Pa only
    client->print("\t\t\t");

    //Heat Index
    client->print(heatIndex);
    client->write(0xC2);
    client->write(0xB0);
    client->print(String(envTempUnit == EnvironmentCalculations::TempUnit_Celsius ? "C" :"F"));
    client->print("\t\t");

    //Absolute Humidity
    client->println(absHum);
}